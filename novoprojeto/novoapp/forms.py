from django import forms

class EstudanteForm(forms.Form):
    nome = forms.CharField(label='Nome', max_length=100)
    rgm = forms.CharField(label='RGM', max_length=8)
    periodo = forms.IntegerField(label='Período')
