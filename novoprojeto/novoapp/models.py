from django.db import models

class Estudante(models.Model):

    rgm = models.CharField(max_length=8)
    nome = models.CharField(max_length=100)
    periodo = models.IntegerField()