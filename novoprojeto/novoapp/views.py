from django.http import HttpResponse
from django.template import loader
from django.shortcuts import get_object_or_404, render

from .models import Estudante
from .forms import EstudanteForm


def index(request):
    lista_de_estudantes = Estudante.objects.all()
    template = loader.get_template('index.html')
    context = {
        'lista_de_estudantes': lista_de_estudantes,
    }
    return HttpResponse(template.render(context, request))


def exibir_estudante(request, id_estudante):
    estudante = get_object_or_404(Estudante, pk=id_estudante)
    return render(request, 'exibir.html', {'estudante': estudante})


def adicionar_estudante(request):
    template = loader.get_template('create.html')

    if request.method == 'POST':
        form = EstudanteForm(request.POST)

        if form.is_valid():
            novo_estudante = Estudante(
                nome = form.cleaned_data.get('nome'),
                rgm = form.cleaned_data.get('rgm'),
                periodo = form.cleaned_data.get('periodo')
            )

            novo_estudante.save()
            context = {'form': EstudanteForm, 'estudante': "Estudante "+novo_estudante.nome+" criado"}
            return HttpResponse(template.render(context, request))

        else:
            return HttpResponse(form.errors)
    
    elif request.method == 'GET':
        context = {'form': EstudanteForm}
        return HttpResponse(template.render(context, request))


def remover_estudante(request, id_estudante):
    template = loader.get_template('delete.html')
    context = {}

    if request.method == "GET":
        estudante = get_object_or_404(Estudante, pk=id_estudante)

        estudante.delete()
        return HttpResponse(template.render(context, request))

    return render(request, "delete.html", context)


def atualizar_estudante(request, id_estudante):
    template = loader.get_template('update.html')
    context = {'form': EstudanteForm}

    estudante = get_object_or_404(Estudante, pk=id_estudante)

    if request.method == "GET":
        context = {'form': EstudanteForm, 'estudante': estudante}
        return HttpResponse(template.render(context, request))

    elif request.method == 'POST':
        form = EstudanteForm(request.POST)

        if form.is_valid():
            estudante.nome = form.cleaned_data.get('nome')
            estudante.rgm = form.cleaned_data.get('rgm')
            estudante.periodo = form.cleaned_data.get('periodo')

            estudante.save()
            context = {'form': EstudanteForm, 'estudante': estudante, 'mensagem': "Estudante Atualizado"}
            return HttpResponse(template.render(context, request))

        else:
            return HttpResponse(form.errors)

    return HttpResponse(template.render(context, request))