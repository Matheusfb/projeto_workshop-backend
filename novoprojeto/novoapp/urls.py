from django.urls import path
from .views import index, exibir_estudante, adicionar_estudante, remover_estudante, atualizar_estudante


urlpatterns = [
    path('estudantes/', index),
    path('estudante/<id_estudante>/', exibir_estudante),
    path('add_estudante/', adicionar_estudante),
    path('estudante/<id_estudante>/remover/', remover_estudante),
    path('estudante/<id_estudante>/atualizar/', atualizar_estudante),
]
